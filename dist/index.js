"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_transport_1 = __importDefault(require("winston-transport"));
const mongodb_1 = require("mongodb");
let client;
class MongoDBTransport extends winston_transport_1.default {
    constructor(opts) {
        super(opts);
        this.message = '';
        this.uri = opts.uri;
        this.options = opts.options;
        this.jobId = opts.jobId;
        this.project = new mongodb_1.ObjectID(opts.project);
    }
    log(info, callback) {
        if (!client) {
            client = mongodb_1.MongoClient.connect(this.uri, this.options);
        }
        client.then(client => {
            // @ts-ignore
            const collection = client.db(client.s.options.dbName).collection('joblogs');
            this.message += info.message + '\n';
            collection
                .updateOne({
                jobId: this.jobId,
                project: this.project
            }, {
                $set: { message: this.message }
            }, {
                upsert: true
            })
                .then(() => {
                this.emit('logged', info);
                callback(null, true);
            })
                .catch(error => {
                this.emit('error', error);
                callback(error);
            });
        });
    }
}
exports.default = MongoDBTransport;
