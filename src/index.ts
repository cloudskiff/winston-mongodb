import Transport from 'winston-transport';
import { ObjectID, MongoClient, MongoClientOptions } from 'mongodb';

let client: Promise<MongoClient>;

export default class MongoDBTransport extends Transport {
  uri: string;
  options: MongoClientOptions;
  jobId: string;
  project: ObjectID;

  message: string = '';

  constructor(opts) {
    super(opts);

    this.uri = opts.uri;
    this.options = opts.options;
    this.jobId = opts.jobId;
    this.project = new ObjectID(opts.project);
  }

  log(info, callback) {
    if (!client) {
      client = MongoClient.connect(this.uri, this.options);
    }

    client.then(client => {
      // @ts-ignore
      const collection = client.db(client.s.options.dbName).collection('joblogs');

      this.message += info.message + '\n';

      collection
        .updateOne(
          {
            jobId: this.jobId,
            project: this.project
          },
          {
            $set: { message: this.message }
          },
          {
            upsert: true
          }
        )
        .then(() => {
          this.emit('logged', info);
          callback(null, true);
        })
        .catch(error => {
          this.emit('error', error);
          callback(error);
        });
    });
  }
}
